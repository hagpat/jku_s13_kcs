% jodici.pl
%
% Jodici (http://www.jodici.at/) wurde 2012 von Herbert Jodlbauer
% (http://research.fh-ooe.at/staff/734) erfunden. Das Rätsel besteht aus
% einem Kreis, der in drei Kreisringe und sechs Kreissektoren aufgeteilt ist.
%
% Regeln:
% Die Zahlen 1-9 sind genau zweimal zu verwenden.
% Jeder der drei Kreisringe muss auf 30 aufsummieren.
% Jeder der sechs Kreissektoren muss auf 15 aufsummieren.
%
% Aufruf:
% jodici([[A,B,C,D,E,F],[G,H,I,J,K,L],[O,P,Q,R,S,T]]).
%
% Aufgaben vom 25. Apr. 2013:
%
% Leicht:    jodici([[A,B,C,D,9,F],[4,7,9,J,5,2],[8,P,Q,4,S,T]]).
% Mittel:    jodici([[A,5,C,D,9,5],[G,H,1,8,K,L],[2,3,Q,R,S,7]]).
% Schwierig: jodici([[A,B,C,D,8,F],[G,5,I,J,3,L],[9,P,2,1,S,6]]).
% Knifflig:  jodici([[A,B,5,D,E,F],[G,H,I,J,4,L],[O,6,4,1,S,9]]).
%
% Aufgaben vom 26. Apr. 2013:
%
% Leicht:    jodici([[A,3,C,D,E,3],[6,H,1,6,9,L],[O,8,Q,R,5,T]]).
% Mittel:    jodici([[A,1,C,D,E,F],[2,H,2,J,K,1],[7,P,7,R,S,T]]).
% Schwierig: jodici([[A,5,C,4,E,3],[1,9,8,J,K,L],[O,P,Q,4,S,T]]).
% Knifflig:  jodici([[A,B,C,6,4,F],[G,H,I,5,K,3],[6,P,1,R,S,T]]).
%
% Aufgaben vom 2. Mai 2013, mit Zeitmessung:
%
% time(jodici([[A,B,C,D,E,7],[9,H,I,J,2,3],[O,6,3,8,4,T]])).
% time(jodici([[2,B,C,3,E,8],[G,H,5,8,K,L],[O,P,Q,R,9,4]])).
% time(jodici([[2,B,C,9,E,F],[G,5,I,J,9,L],[O,7,Q,1,S,4]])).
% time(jodici([[A,B,C,D,E,6],[3,H,I,J,9,3],[O,P,1,7,S,T]])).
%

sektor([1,5,9]).
sektor([1,6,8]).
sektor([1,7,7]).
sektor([1,8,6]).
sektor([1,9,5]).

sektor([2,4,9]).
sektor([2,5,8]).
sektor([2,6,7]).
sektor([2,7,6]).
sektor([2,8,5]).
sektor([2,9,4]).

sektor([3,3,9]).
sektor([3,4,8]).
sektor([3,5,7]).
sektor([3,6,6]).
sektor([3,7,5]).
sektor([3,8,4]).
sektor([3,9,3]).

sektor([4,2,9]).
sektor([4,3,8]).
sektor([4,4,7]).
sektor([4,5,6]).
sektor([4,6,5]).
sektor([4,7,4]).
sektor([4,8,3]).
sektor([4,9,2]).

sektor([5,1,9]).
sektor([5,2,8]).
sektor([5,3,7]).
sektor([5,4,6]).
sektor([5,5,5]).
sektor([5,6,4]).
sektor([5,7,3]).
sektor([5,8,2]).
sektor([5,9,1]).

sektor([6,1,8]).
sektor([6,2,7]).
sektor([6,3,6]).
sektor([6,4,5]).
sektor([6,5,4]).
sektor([6,6,3]).
sektor([6,7,2]).
sektor([6,8,1]).

sektor([7,1,7]).
sektor([7,2,6]).
sektor([7,3,5]).
sektor([7,4,4]).
sektor([7,5,3]).
sektor([7,6,2]).
sektor([7,7,1]).

sektor([8,1,6]).
sektor([8,2,5]).
sektor([8,3,4]).
sektor([8,4,3]).
sektor([8,5,2]).
sektor([8,6,1]).

sektor([9,1,5]).
sektor([9,2,4]).
sektor([9,3,3]).
sektor([9,4,2]).
sektor([9,5,1]).

anzahl(_, [], 0).
anzahl(X, [X|Rest], N) :- 
    anzahl(X, Rest, Nminus1), 
    !,
    succ(Nminus1, N).
anzahl(X, [Not_X|Rest], N) :- 
	anzahl(X, Rest, N).

summe([A,B,C,D,E,F], N) :- 
    N is A + B + C + D + E + F.

jodici([[A,B,C,D,E,F],[G,H,I,J,K,L],[O,P,Q,R,S,T]]) :- 
    %generate
	sektor([A,G,O]),
	sektor([B,H,P]),
	sektor([C,I,Q]),
	sektor([D,J,R]),
	sektor([E,K,S]),
	sektor([F,L,T]),
	%test
	summe([A,B,C,D,E,F], 30),
	summe([G,H,I,J,K,L], 30),
	summe([O,P,Q,R,S,T], 30),
	anzahl(1, [A,B,C,D,E,F,G,H,I,J,K,L,O,P,Q,R,S,T], 2),
	anzahl(2, [A,B,C,D,E,F,G,H,I,J,K,L,O,P,Q,R,S,T], 2),
	anzahl(3, [A,B,C,D,E,F,G,H,I,J,K,L,O,P,Q,R,S,T], 2),
	anzahl(4, [A,B,C,D,E,F,G,H,I,J,K,L,O,P,Q,R,S,T], 2),
	anzahl(5, [A,B,C,D,E,F,G,H,I,J,K,L,O,P,Q,R,S,T], 2),
	anzahl(6, [A,B,C,D,E,F,G,H,I,J,K,L,O,P,Q,R,S,T], 2),
	anzahl(7, [A,B,C,D,E,F,G,H,I,J,K,L,O,P,Q,R,S,T], 2),
	anzahl(8, [A,B,C,D,E,F,G,H,I,J,K,L,O,P,Q,R,S,T], 2),
	anzahl(9, [A,B,C,D,E,F,G,H,I,J,K,L,O,P,Q,R,S,T], 2),
	%print
	print_jodici([A,B,C,D,E,F,G,H,I,J,K,L,O,P,Q,R,S,T]).

print_jodici([A,B,C,D,E,F,G,H,I,J,K,L,O,P,Q,R,S,T]) :-
    write('      '), write(A), nl,
    write(F), write('     '), write(G), write('      '), write(B), nl,
    write('  '), write(L), write('   '), write(O), write('   '), write(H), nl,
    write('    '), write(T), write('   '), write(P), nl,
    write('    '), write(S), write('   '), write(Q), nl,
    write('  '), write(K), write('   '), write(R), write('   '), write(I), nl,
    write(E), write('     '), write(J), write('      '), write(C), nl,
    write('      '), write(D), nl.
